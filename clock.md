  

# Doc. Clock Clocks Docs

  

## By Bohrium

  

{% alert warning %}Italicized days are for the future{% endalert %}

  

*If you are Bohring, then get better*

### Chapter 1: March

  
  

3/16/2022: Day 0: Whoops I am bohring … but how to get better?

  

3/17/2022: Day 1: Just Do It

  

3/18/2022: Day 2: :):(:)(

  

3/19/2022: Day 3: Increasing Entropy …

  

3/20/2022: Day 4: ½a(Δt)²

  

3/21/2022: Day 5: Robert’); DROP TABLE students;

  
  

3/22/2022: Day 6: Sanitize database inputs

  

3/23/2022: Day 7: K-Pop ... How and Why did I get into it?

  

3/24/2022: Day 8: Stan ENHYPEN

  

3/25/2022: Day 9: UIL coming fast ... Project Melody

  

{% alert warning %}Day 10 was lost and therefore is not recorded.{% endalert %}

  

3/26/2022: Day 10: `Lost to History`

  
  

3/27/2022: Day 11: Not IZ*ONE

  

3/28/2022: Day 12: Drunk-Dazed computer visualization ... computer = drunk + dazed :)

  

3/29/2022: Day 13: Dust <--> Original Sin?

  
  
  

3/30/2022: Day 14: You are a 멍청한사람 if you think I will call JYP for you. Do it yourself.

  

3/31/2022: Day 15: DEATH TO THE FIREMEN!!!! Death to the UIL S&L judges!!!

  

### Chapter 2: April

  

4/1/2022: Day 16: `Lost to History`

  

4/2/2022: Day 17: Bohr-odin?

  

**4/3/2022: Day 18: I failed a test**

  

4/4/2022: Day 19: TODO

### Chapter 2.5: April Freedom

**4/5/2022: Day 20: TODO**

  

*4/6/2022: Day 21: TODO*

  

*4/7/2022: Day 22: TODO*

  

*4/8/2022: Day 23: TODO*

  

*4/9/2022: Day 24: TODO*

 {% alert danger %} The following dates are far in the future {% endalert %} 

4/10/2022: Day 25: TODO
  
4/11/2022: Day 26: TODO
  
4/12/2022: Day 27:
  
4/13/2022: Day 28:
  
4/14/2022: Day 29:
  
4/15/2022: Day 30:
  
4/16/2022: Day 31:
  
4/17/2022: Day 32:
  
4/18/2022: Day 33:
  
4/19/2022: Day 34:
  
4/20/2022: Day 35:
  
4/21/2022: Day 36:
  
4/22/2022: Day 37:
  
4/23/2022: Day 38:
  
4/24/2022: Day 39:
  
4/25/2022: Day 40:
  
4/26/2022: Day 41:
  
4/27/2022: Day 42:
  
4/28/2022: Day 43:
  
4/29/2022: Day 44:
  
4/30/2022: Day 45:

  

### Chapter 3: May

  

*5/1/2022: Day 46*
