# Blog

## Posts

{% for post in site.posts %}
* [{{post.title}}]({{post.url}})
{% endfor %}



## Authors


{% for author in site.author %}
* [{{author.display_name}}]({{author.url}})
{%endfor%}
