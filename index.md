# Dalton's Site

## Announcements

**IMPORTANT: I WILL NOT UPDATE THIS SITE AS I HAVE BEEN LOCKED OUT OF GITLAB.**


**Check out my [Clock](clock)**




-----

## Blog

{% for post in site.posts %}
* {{ post.date | date: "%b %-d, %Y" }}  [{{post.title}}]({{post.url | prepend: site.baseurl}})
{% endfor %}
