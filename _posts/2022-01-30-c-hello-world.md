---
layout: post
title:  "C Hello world program"
date:   2022-01-30
categories: c
author: dalton
---

# Hello, world 


**Full code:**

```c
#include <stdio.h>

int main(void) {
    printf("Hello, world");
}
```