---
layout: post
title:  "C vs Scratch"
date:   2022-01-29
categories: c
author: dalton
---

C:

```c
printf("Hello, world\n");
```

Scratch: 
```scratch
when green flag clicked
say [Hello, world]
```

Output: 

*(We aren't showing scratch output)*

```
$ ./hello
Hello, world
$ 
```